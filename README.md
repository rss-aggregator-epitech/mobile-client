
<div align='center'>

# RSS Aggregator - Mobile

[![pipeline status](https://gitlab.com/rss-aggregator-epitech/mobile-client/badges/master/pipeline.svg)](https://gitlab.com/rss-aggregator-epitech/mobile-client/-/commits/master)

</div>

RSS Aggregator is an Epitech project with 4 projects:
- [A Server](https://gitlab.com/rss-aggregator-epitech/server): built with [Keystone.js](https://www.keystonejs.com/) with [GraphQL](https://graphql.org/) and [MongoDB Atlas](https://www.mongodb.com/cloud/atlas).
- [A WEB Client](https://gitlab.com/rss-aggregator-epitech/web-client): built with [React](https://fr.reactjs.org/)
- [A Mobile Client](https://gitlab.com/rss-aggregator-epitech/mobile-client): built with [Expo](https://expo.io/) and [React Native](https://reactnative.dev/)
- [A Desktop Client](https://gitlab.com/rss-aggregator-epitech/desktop-client): build with [Electro.js](https://www.electronjs.org/) and [React](https://fr.reactjs.org/)

All projects are deployed in [Heroku](https://www.heroku.com/).

Next to the server, a Keystone BackOffice is deployed to manipulate datas and a [GraphiQL Playground](https://github.com/graphql/graphiql) is deployed to browse and test the API.

All staging/production links are listed bellow in the [Production](#production) section.

> All accesses to MongoDB Atlas / Google Console / Heroku must be added manually, send an email to Quentin KERMAIDIC at <quentin.kermaidic@epitech.eu>

> Note that only trusted GMail accounts can use Google Auth (it must be added manually aswell)
>
> A GMail account has been created for the project and is already authorized
>
> - Email: `test.rss.aggregator@gmail.com`
> 
> - Password: `zA9AR67DfEEB7B6`

## Getting Started - Mobile

### Prerequisite

- **npm**: v7.9.0 *or hight*
- **node**: v15.3.0 *or hight*
- **expo-cli**: v4.4.3 *or hight* (advised: `npm install --global expo-cli `)

Download the application [Expo Go](https://expo.io/client) on your phone/emulator

### Install and run

- Install dependencies: `npm i`
- Run with production server: `npm start` 
- Run with local server: `npm run dev`
- Re-generate types and hooks: `npm run generate`

> You can use your phone directly (scan the QR code) or select your emulator with the Expo CLI

> The types and hooks will be automatically generated on start/dev commands

### GitHooks

- Bump version: `npm run bump`

> On *pre-commit*, commands `npm run bump` is automatically executed with an interactive shell

> You can disable GitHook with: `git commit [options] ... --no-verify` but it is NOT advised

## CI/CD - Mobile

The steps (defined in `.gitlab-ci.yml`) are the following

- Login to production Expo Account
- Build and publish to Expo (only for `staging` and `master` branch)
- Generate and push the new URL to the server (only for `staging` and `master` branch)

## Production

Before production, all projects are deployed in a staging environment (they use different databases and Heroku endpoint, the rest is identical):
- WEB Application: [Production](https://rss-aggregator-web-prod.herokuapp.com/) / [Staging](https://rss-aggregator-web-staging.herokuapp.com/)
- Keystone BackOffice: [Production](https://rss-aggregator-server-prod.herokuapp.com/admin) / [Staging](https://rss-aggregator-server-staging.herokuapp.com/admin)
  - Email: `admin@rss-aggregator.com`
  - Password: `azertyuiop`
- GraphiQL: [Production](https://rss-aggregator-server-prod.herokuapp.com/admin/graphiql) / [Staging](https://rss-aggregator-server-staging.herokuapp.com/admin/graphiql)

![alt-text](./API.png)

For authenticated request make sure to add the following headers:
```
{
  "authorization": "Bearer {token}"
}
```

> Replace the `{token}` with the token from a `signIn` mutation