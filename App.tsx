import * as React from 'react'

import { createDrawerNavigator } from '@react-navigation/drawer'
import {ApolloContextProvider, useApollo} from './contexts/ApolloContext'
import {RooteScreen} from "./screens/RouteScreen";

const Drawer = createDrawerNavigator();

export default function App() {
    return (
        <ApolloContextProvider>
            <RooteScreen/>
        </ApolloContextProvider>
    );
}
