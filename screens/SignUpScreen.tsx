import { LinearGradient } from 'expo-linear-gradient'
import * as React from 'react'
import {
    ActivityIndicator,
    Button, Dimensions, Platform, StatusBar, StyleSheet, Text, TextInput, TouchableOpacity, View,
} from 'react-native'
import * as Animatable from 'react-native-animatable'

import { Feather, FontAwesome } from '@expo/vector-icons'

import {useSignInMutation, useSignUpMutation} from '../generated/hooks'
import {ApolloContext, useApollo} from "../contexts/ApolloContext";

export const SignUpScreen = ({navigation}) => {
    const [signUp] = useSignUpMutation()
    const [signIn] = useSignInMutation()
    let apollo = useApollo();

    const [data, setData] = React.useState({
        email: '', password: '', confirm_password: '', firstname: '', lastname: '',
        check_textInputChange: false, secureTextEntry: true,
        confirmSecureTextEntry: true, ErrorPasswordConfirm: false,
        ErrorServer: false, FormIncompleted: false, isLoading: false
    });

    // Example signUp mutation
    const onSubmit = async () => {
        if (!isFormCompleted()) {
            setData({
                ...data,
                FormIncompleted: true
            })
            return;
        }
        let res = null;
        try {
            setData({
                ...data,
                isLoading: true
            })
            console.log(apollo.isSignedIn);
            res = await signUp({
                variables: {
                    firstname: data.firstname,
                    lastname: data.lastname,
                    email: data.email,
                    password: data.password
                }
            })
        } catch (e) {
            console.log(e);
            setData({
                ...data,
                isLoading: false
            })
            return;
        }
        try {
            res = await signIn({ variables: { email: data.email, password: data.password } })
        } catch (e) {
            console.log(e);
        }
        apollo.setToken(res.data?.authenticateUserWithPassword?.token);
        // console.log('connected with user', res.data?.authenticateUserWithPassword?.item?.email);
        setData({
            ...data,
            isLoading: false
        })

        console.log('connected with token', res.data?.createUser);
    }
    

    const textInputChange = (val) => {
        if (val.length != 0) {
            setData({
                ...data,
                email: val,
                check_textInputChange: true,
                FormIncompleted: false
            })
        } else {
            setData({
                ...data,
                email: val,
                check_textInputChange: false,
                FormIncompleted: false
            })
        }
    };

    const handleFirstnameChange = (val) => {
        setData({
            ...data,
            firstname: val,
            FormIncompleted: false
        })
    }

    const handleLastnameChange = (val) => {
        setData({
            ...data,
            lastname: val,
            FormIncompleted: false
        })
    }

    const handlePasswordChange = (val) => {
        setData({
            ...data,
            password: val,
            FormIncompleted: false
        })
    }

    const handleConfirmPasswordChange = (val) => {
        setData({
            ...data,
            confirm_password: val,
            FormIncompleted: false
        })
    }

    const updateSecureTextEntry = () => {
        setData({
            ...data,
            secureTextEntry: !data.secureTextEntry
        })
    }

    const updateConfirmSecureTextEntry = () => {
        setData({
            ...data,
            confirmSecureTextEntry: !data.confirmSecureTextEntry
        })
    }

    const isFormCompleted = () => {
        if (data.password != data.confirm_password) {
            setData({
                ...data,
                ErrorPasswordConfirm: true
            })
        }
        if (data.email == '' || data.password == '' || data.firstname == '' || data.lastname == '') {
            return false;
        } else {
            return true;
        }
    }

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor={'#009387'} barStyle={'default'}/>
            <View style={styles.header}>
                <Text style={styles.text_header}>Register now !</Text>
            </View>
            <Animatable.View animation={"fadeInUpBig"} style={styles.footer}>
                <Text style={styles.text_footer}>Firstname</Text>
                <View style={styles.action}>
                    <FontAwesome name={'user-o'} color={"#05375a"} size={20}/>
                    <TextInput
                        placeholder={"Your Firstname"}
                        style={styles.textInput}
                        autoCapitalize={'none'}
                        onChangeText={(val) => handleFirstnameChange(val)}
                    />
                </View>
                <Text style={[styles.text_footer, {marginTop: 20}]}>Lastname</Text>
                <View style={styles.action}>
                    <FontAwesome name={'user-o'} color={"#05375a"} size={20}/>
                    <TextInput
                        placeholder={"Your Lastname"}
                        style={styles.textInput}
                        autoCapitalize={'none'}
                        onChangeText={(val) => handleLastnameChange(val)}
                    />
                </View>
                <Text style={[styles.text_footer, {marginTop: 20}]}>Email</Text>
                <View style={styles.action}>
                    <FontAwesome name={'user-o'} color={"#05375a"} size={20}/>
                    <TextInput
                        placeholder={"Your email"}
                        style={styles.textInput}
                        autoCapitalize={'none'}
                        onChangeText={(val) => textInputChange(val)}
                    />
                    {data.check_textInputChange ?
                        <Animatable.View animation={"bounceIn"}>
                            <Feather name={'check-circle'} color={'green'} size={20}/>
                        </Animatable.View>
                        : null}
                </View>
                <Text style={[styles.text_footer, {marginTop: 20}]}>Password</Text>
                <View style={styles.action}>
                    <Feather name={'lock'} color={"#05375a"} size={20}/>
                    <TextInput
                        placeholder={"Your password"}
                        secureTextEntry={data.secureTextEntry ? true : false}
                        style={styles.textInput}
                        autoCapitalize={'none'}
                        onChangeText={(val) => handlePasswordChange(val)}
                    />
                    <TouchableOpacity onPress={updateSecureTextEntry}>
                        {data.secureTextEntry ?
                            <Feather name={'eye-off'} color={'green'} size={20}/>
                            : <Feather name={'eye'} color={'green'} size={20}/>}
                    </TouchableOpacity>
                </View>
                <Text style={[styles.text_footer, {marginTop: 20}]}>Confirm Password</Text>
                <View style={styles.action}>
                    <Feather name={'lock'} color={"#05375a"} size={20}/>
                    <TextInput
                        placeholder={"Your password"}
                        secureTextEntry={data.confirmSecureTextEntry ? true : false}
                        style={styles.textInput}
                        autoCapitalize={'none'}
                        onChangeText={(val) => handleConfirmPasswordChange(val)}
                    />
                    <TouchableOpacity onPress={updateConfirmSecureTextEntry}>
                        {data.confirmSecureTextEntry ?
                            <Feather name={'eye-off'} color={'green'} size={20}/>
                            : <Feather name={'eye'} color={'green'} size={20}/>}
                    </TouchableOpacity>
                </View>
                {data.ErrorPasswordConfirm ?
                    <View style={{marginTop: 10}}>
                        <Text style={{color: 'red', textAlign: 'center'}}>Passwords doesn't match !</Text>
                    </View> : null}
                {data.FormIncompleted ?
                    <View style={{marginTop: 10}}>
                        <Text style={{color: 'red', textAlign: 'center'}}>You must complete every input !</Text>
                    </View> : null}
                {data.isLoading ?
                    <View style={{flex:1, justifyContent: 'center', alignItems: 'center'}}>
                        <ActivityIndicator size="large" color="#009387"/>
                    </View>: null}
                <View style={styles.button}>
                    <TouchableOpacity style={styles.signIn} onPress={onSubmit}>
                        <LinearGradient colors={['#08d4c4', '#01ab9d']}
                                        style={styles.signIn}>
                            <Text style={styles.textSign}>Sign up</Text>
                        </LinearGradient>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => navigation.goBack()}
                                      style={[styles.signIn, {
                                          borderColor: '#009387',
                                          borderWidth: 1,
                                          marginTop: 15}]}>
                        <Text style={styles.textSign}>Sign in</Text>
                    </TouchableOpacity>
                </View>
            </Animatable.View>

        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#009387'
    },
    header: {
        flex: 0.5,
        justifyContent: 'flex-end',
        paddingHorizontal: 20,
        paddingBottom: 20
    },
    footer: {
        flex: 3,
        backgroundColor: '#fff',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingHorizontal: 20,
        paddingVertical: 30
    },
    text_header: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 30
    },
    text_footer: {
        color: '#05375a',
        fontSize: 18
    },
    action: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingBottom: 5
    },
    actionError: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#FF0000',
        paddingBottom: 5
    },
    textInput: {
        flex: 1,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: '#05375a',
    },
    errorMsg: {
        color: '#FF0000',
        fontSize: 14,
    },
    button: {
        alignItems: 'center',
        marginTop: 50
    },
    signIn: {
        width: '100%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10
    },
    textSign: {
        fontSize: 18,
        fontWeight: 'bold'
    }
});
