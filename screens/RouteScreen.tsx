import * as React from "react";

import {createStackNavigator} from "@react-navigation/stack";
import {SignInScreen} from "./SignInScreen";
import {SplashScreen} from "./SplashScreen";
import {SignUpScreen} from "./SignUpScreen";
import {HomeScreen} from "./HomeScreen";
import {DetailsScreen} from "./DetailsScreen";
import {MaintabScreen} from "./MainTabScreen";
import {createDrawerNavigator} from "@react-navigation/drawer";
import {useApollo} from "../contexts/ApolloContext";
import {DrawerContent} from "./DrawerContent";
import {ProfileScreen} from "./ProfileScreen";
import {RootStackScreen} from "./RootStackScreen";
import {NavigationContainer} from "@react-navigation/native";
import {SettingsScreen} from "./SettingsScreen";
import {FeedScreen} from "./FeedScreen";

const RootStack = createStackNavigator();
const Drawer = createDrawerNavigator()

export const RooteScreen = ({navigation}) => {

    let {isSignedIn, removeToken} = useApollo();

    return (
        <NavigationContainer>
            {!isSignedIn ?
                <RootStackScreen/>
                :
                <Drawer.Navigator drawerContent={props => <DrawerContent {...props} />}>
                    <Drawer.Screen name="HomeDrawer" component={MaintabScreen}/>
                    <Drawer.Screen name="ProfileScreen" component={ProfileScreen}/>
                    <Drawer.Screen name="FeedScreen" component={FeedScreen}/>
                    <Drawer.Screen name="HomeScreen" component={HomeScreen}/>
                </Drawer.Navigator>
            }
        </NavigationContainer>
        )
};
