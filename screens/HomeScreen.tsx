import * as React from "react";
import {createStackNavigator} from "@react-navigation/stack";
import {HomeAllFeedsScreen} from "./HomeAllFeedsScreen";
import {FeedArticleScreen} from "./FeedArticleScreen";

const Stack = createStackNavigator();

export const HomeScreen = ({navigation}) => {

    return (
        <Stack.Navigator screenOptions={{
            headerTintColor: '#fff'
        }
        }>
            <Stack.Screen name={"Visualize all feeds"} component={HomeAllFeedsScreen}/>
            <Stack.Screen name={"Visualize article"} component={FeedArticleScreen}/>
        </Stack.Navigator>
    )
}
