import {Button, Text, TextInput, View, StyleSheet, Platform, TouchableOpacity, ActivityIndicator} from "react-native";
import * as React from "react";
import {Feather, FontAwesome} from "@expo/vector-icons";
import * as Animatable from "react-native-animatable";
import {useCurrentUserQuery, useUpdateUserMutation} from "../generated/hooks";

export const ProfileScreen = ({navigation}) => {

    const {data, loading, error} = useCurrentUserQuery({variables: {}})
    const forceUpdate: () => void = React.useState()[1].bind(null, {})
    const [updateUser] = useUpdateUserMutation();
    const [entries, setEntries] = React.useState({
        firstname: data?.authenticatedUser?.firstname,
        lastname: data?.authenticatedUser?.lastname,
        email: data?.authenticatedUser?.email,
        isLoading: false
    })

    const handleFirstNameChange = (val) => {
        if (val.length != 0) {
            setEntries({
                ...entries,
                firstname: val
            })
        }
    }

    const handleLastNameChange = (val) => {
        if (val.length != 0) {
            setEntries({
                ...entries,
                lastname: val
            })
        }
    }

    const handleEmailChange = (val) => {
        if (val.length != 0) {
            setEntries({
                ...entries,
                email: val
            })
        }
    }

    const onSubmit = async () => {
        let res = null;
        try {
            setEntries({
                ...entries,
                isLoading: true
            })
            res = await updateUser({
                variables: {
                    id: data?.authenticatedUser?.id,
                    firstname: entries.firstname,
                    lastname: entries.lastname,
                    email: entries.email
                }
            })
        } catch (e) {
            setEntries({
                ...entries,
                isLoading: false
            })
            console.log(e);
        }
        setEntries({
            ...entries,
            isLoading: false
        })
        forceUpdate();
    }

    return (
        <View style={styles.container}>
            <Text style={styles.text_header}>Change User Infomations</Text>
            {entries.isLoading ?
                <View style={{flex:1, justifyContent: 'center', alignItems: 'center'}}>
                    <ActivityIndicator size="large" color="#009387"/>
                </View>: null}
            <Animatable.View animation={"fadeInUpBig"} style={styles.footer}>
                <Text style={styles.text_footer}>Firstname</Text>
                <View style={styles.action}>
                    <FontAwesome name={'user-o'} color={"#05375a"} size={20}/>
                    <TextInput
                        placeholder={'Current is ' + data?.authenticatedUser?.firstname}
                        style={styles.textInput}
                        autoCapitalize={'none'}
                        onChangeText={(val) => {handleFirstNameChange(val)}}
                    />
                </View>
                <Text style={[styles.text_footer, {marginTop: 10}]}>Lastname</Text>
                <View style={styles.action}>
                    <FontAwesome name={'user-o'} color={"#05375a"} size={20}/>
                    <TextInput
                        placeholder={'Current is ' + data?.authenticatedUser?.lastname}
                        style={styles.textInput}
                        autoCapitalize={'none'}
                        onChangeText={(val) => {handleLastNameChange(val)}}
                    />
                </View>
                <Text style={[styles.text_footer, {marginTop: 10}]}>Email</Text>
                <View style={styles.action}>
                    <FontAwesome name={'user-o'} color={"#05375a"} size={20}/>
                    <TextInput
                        placeholder={'Current is ' + data?.authenticatedUser?.email}
                        style={styles.textInput}
                        autoCapitalize={'none'}
                        onChangeText={(val) => {handleEmailChange(val)}}
                    />
                </View>
                <TouchableOpacity onPress={onSubmit}
                                  style={[styles.btn, {
                                      borderColor: '#009387',
                                      borderWidth: 1,
                                      marginTop: 15}]}>
                    <Text style={styles.text_btn}>Apply changes</Text>
                </TouchableOpacity>
            </Animatable.View>


        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 20
    },
    title: {
        textAlign: 'center',
        fontSize: 30
    },
    text_header: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 30
    },
    btn: {
        width: '100%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10
    },
    text_btn: {
        fontSize: 18,
        fontWeight: 'bold'
    },
    label: {
        textAlign: 'left',
        margin: 5,
        marginTop: 10,
        fontSize: 20
    },
    textInput: {
        flex: 1,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: '#05375a',
    },
    action: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingBottom: 5
    },
    text_footer: {
        color: '#05375a',
        fontSize: 18
    },
    footer: {
        flex: 3,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingHorizontal: 20,
        paddingVertical: 30
    },
})
