import {ActivityIndicator, FlatList, Picker, SafeAreaView, StyleSheet, Text, View} from "react-native";
import * as React from "react";
import {useApollo} from "../contexts/ApolloContext";
import {useCurrentUserQuery, useGetAllTagsQuery, useGetUserFeedByIdQuery} from "../generated/hooks";
import {Article, Tag, TagFragment, WebsiteFragment} from "../generated/types";
import { Avatar, Button, Card, Title, Paragraph, TextInput } from 'react-native-paper';

interface Website {
    id: string,
    title: string,
    articles: Article[],
}

const wait = (timeout) => {
    return new Promise(resolve => setTimeout(resolve, timeout));
}

export const FeedSeeScreen = ({route, navigation}) => {

    const params = route.params;
    const [websites, setWebsites] = React.useState<WebsiteFragment[]>([])
    const [refreshing, setRefreshing] = React.useState(false);
    const [entries, setEntries] = React.useState({
        search: '', websites_name: 'all', websites_array: [], isLoading: true
    });
    const {data, refetch} = useGetUserFeedByIdQuery({
        variables: {
            feedId: params.feedId
        },
        fetchPolicy: 'no-cache'
    });

    React.useEffect(() => {
        if (!data || !data.Feed?.websites) return;

        let docs: WebsiteFragment[] = [];
        let websites_names : string[] = [];

        for (const item of data?.Feed?.websites) {
            if (typeof (item.title) != 'undefined') {
                if (!websites_names.includes(item.title)) {
                    websites_names.push(item.title);
                }
            }
        }
        docs = [...docs, ...data?.Feed?.websites];
        if (docs.length > 0) {
            setEntries({
                ...entries,
                isLoading: false,
                websites_array: websites_names
            })
        }
        setWebsites(docs);
    },[data])

    const onRefresh = React.useCallback(async () => {
        setRefreshing(true);
        await refetch()
        wait(2000).then(() => setRefreshing(false))
    }, [])

    const renderItem = ({item}) => {
        if (entries.websites_name == 'all' || entries.websites_name == item.title) {
            return (
                <View>
                    {item.articles.map((props, key) => {
                        if (props.title.toLowerCase().includes(entries.search.toLowerCase()) || entries.search == '') {
                            return (
                                <View key={key} style={{borderStyle: 'dashed', borderColor: '#DCDCDC'}}>
                                    <Card>
                                        <Card.Title title={item.title}/>
                                        <Card.Content>
                                            <Title>{props.title}</Title>
                                            <Title>{props.read}</Title>
                                            <Paragraph>{props.contentSnippet.slice(0, 50)}</Paragraph>
                                        </Card.Content>
                                        {}
                                        {/*<Card.Cover source={{uri: props.image}} />*/}
                                        <Card.Actions>
                                            <Button onPress={() => {
                                                navigation.navigate('Visualize article', {
                                                    article: props,
                                                    website_id: item.id
                                                })
                                            }}>See article</Button>
                                            {props.read ?
                                                <Text>Already Read</Text>
                                                : null}
                                        </Card.Actions>
                                    </Card>
                                </View>

                            )
                        } else {
                            return (null);
                        }
                    })}
                </View>
            )
        } else {
            return (null);
        }
    }

    const handleSearchInput = (val) => {
        setEntries({
            ...entries,
            search: val
        })
    }

    const selectWebsiteHandler = (val) => {
        setEntries({
            ...entries,
            websites_name: val
        })
    }



    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.container}>
                <TextInput placeholder={"Search for an article"} onChangeText={(val) => handleSearchInput(val)}/>
                {entries.websites_array.length > 0 ?
                <Picker selectedValue={entries.websites_name} style={{alignSelf: 'stretch'}} onValueChange={(val) => selectWebsiteHandler(val)}>
                    <Picker.Item label={"All"} value={"all"}/>
                    {entries.websites_array.map((item, key) => {
                        return (
                            <Picker.Item key={key} label={item} value={item}/>
                        );
                    })}
                </Picker>:null}
                {entries.isLoading ?
                    <View style={{flex:1, justifyContent: 'center', alignItems: 'center'}}>
                        <ActivityIndicator size="large" color="#009387"/>
                        <Text>Waiting for articles. Ensure that you subscribed to a website</Text>
                    </View>: null}
                {websites.length > 0 ?
                <FlatList
                    data={websites}
                    renderItem={renderItem}
                    keyExtractor={item => item.id}
                />: null}
            </View>
        </SafeAreaView>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#696969',
        padding: 10,
    },
    titleStyle: {
        fontSize: 28,
        fontWeight: 'bold',
        textAlign: 'center',
        padding: 10,
    },
    textStyle: {
        fontSize: 16,
        textAlign: 'center',
        padding: 10,
    },
    touchableOpacityStyle: {
        position: 'absolute',
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        right: 30,
        bottom: 30,
    },
    touchableOpacity2Style: {
        position: 'absolute',
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        left: 30,
        bottom: 30,
    },
    floatingButtonStyle: {
        resizeMode: 'contain',
        width: 50,
        height: 50,
        //backgroundColor:'black'
    },
    item: {
        backgroundColor: '#009387',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
        height: 80
    },
    title: {
        fontSize: 32,
    },
});
