import * as React from "react";

import {createStackNavigator} from "@react-navigation/stack";
import {SignInScreen} from "./SignInScreen";
import {SplashScreen} from "./SplashScreen";
import {SignUpScreen} from "./SignUpScreen";
import {HomeScreen} from "./HomeScreen";
import {DetailsScreen} from "./DetailsScreen";
import {MaintabScreen} from "./MainTabScreen";
import {createDrawerNavigator} from "@react-navigation/drawer";
import {useApollo} from "../contexts/ApolloContext";
import {DrawerContent} from "./DrawerContent";
import {ProfileScreen} from "./ProfileScreen";
import {ExploreScreen} from "./ExploreScreen";

const RootStack = createStackNavigator();
const Drawer = createDrawerNavigator()

export const RootStackScreen = ({navigation}) => {

    let {isSignedIn} = useApollo();

    return (
        <RootStack.Navigator headerMode={'none'}>
            <RootStack.Screen name={"SplashScreen"} component={SplashScreen}/>
            <RootStack.Screen name={"SignInScreen"} component={SignInScreen}/>
            <RootStack.Screen name={"SignUpScreen"} component={SignUpScreen}/>
        </RootStack.Navigator>
    )
};
