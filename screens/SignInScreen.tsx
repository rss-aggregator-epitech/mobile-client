import * as Google from 'expo-auth-session/providers/google'
import { LinearGradient } from 'expo-linear-gradient'
import * as React from 'react'
import {
    ActivityIndicator, Platform, StatusBar, StyleSheet, Text, TextInput, TouchableOpacity, View,
} from 'react-native'
import * as Animatable from 'react-native-animatable'

import { Feather, FontAwesome } from '@expo/vector-icons'

import { GOOGLE_EXPO_CLIENT_ID } from '../constants'
import { useApollo } from '../contexts/ApolloContext'
import { useSignInMutation, useSignInOrSignUpWithGoogleMutation } from '../generated/hooks'

export const SignInScreen = ({navigation}) => {
    const [signIn] = useSignInMutation()
    let apollo = useApollo();
    const [signInOrSignUpWithGoogle] = useSignInOrSignUpWithGoogleMutation()
    const [isGoogleAvailable, responseGoogleAuth, signUpOrSignInWithGoogle] = Google.useAuthRequest({
        expoClientId: GOOGLE_EXPO_CLIENT_ID,
    })
    
    const [data, setData] = React.useState({
        email: '', password: '', check_textInputChange: false, secureTextEntry: true, formIsCompletedError: false, CredentialError: false, isLoading: false
    });

    // Example signIn mutation
    const onSubmitEmailAuth = async () => {
        if (!isFormCompleted()) {
            setData({
                ...data,
                formIsCompletedError: true
            })
            return;
        }
        let res;
        try {
            setData({
                ...data,
                isLoading: true
            });
            res = await signIn({ variables: { email: data.email, password: data.password } })
        } catch (e) {
            console.log(e);
            setData({
                ...data,
                CredentialError: true,
                isLoading: false
            });
            return;
        }


        setData({
            ...data,
            isLoading: false
        });

        apollo.setToken(res.data?.authenticateUserWithPassword?.token);
    }

    // Example sign in or sign up with google
    const onSubmitGoogleAuth = async () => {
        let res = null;
        if (isGoogleAvailable) {
            try {
                res = await signUpOrSignInWithGoogle();
            } catch (e) {
                console.log(e);
            }
        } else {
            console.log('network error')
        }
    }

    // Handle the Google response 
    const onResponseGoogleAuth = async () => {
        if (responseGoogleAuth?.type === 'success') {
            const res = await signInOrSignUpWithGoogle({
                variables: {
                    token: responseGoogleAuth.params.access_token
                }
            })

            apollo.setToken(res.data?.authenticateUserWithGoogle?.token);
        }
    }

    // Listen for Google response
    React.useEffect(() => {
        onResponseGoogleAuth()
    }, [responseGoogleAuth])

    const textInputChange = (val) => {
        if (val.length != 0) {
            setData({
                ...data,
                email: val,
                check_textInputChange: true,
                formIsCompletedError: false,
                CredentialError: false
            })
        } else {
            setData({
                ...data,
                email: val,
                check_textInputChange: false,
                formIsCompletedError: false,
                CredentialError: false
            })
        }
    };

    const handlePasswordChange = (val) => {
        setData({
            ...data,
            password: val,
            formIsCompletedError: false,
            CredentialError: false
        })
    }

    const updateSecureTextEntry = () => {
        setData({
            ...data,
            secureTextEntry: !data.secureTextEntry
        })
    }

    const isFormCompleted = () => {
        if (data.email == '' || data.password == '') {
            return false;
        } else {
            return true;
        }
    }

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor={'#009387'} barStyle={'default'}/>
            <View style={styles.header}>
                <Text style={styles.text_header}>Welcome !</Text>
            </View>
            <Animatable.View animation={"fadeInUpBig"} style={styles.footer}>
                <Text style={styles.text_footer}>Email</Text>
                <View style={styles.action}>
                    <FontAwesome name={'user-o'} color={"#05375a"} size={20}/>
                    <TextInput
                        placeholder={"Your email"}
                        style={styles.textInput}
                        autoCapitalize={'none'}
                        onChangeText={(val) => textInputChange(val)}
                    />
                    {data.check_textInputChange ?
                        <Animatable.View animation={"bounceIn"}>
                            <Feather name={'check-circle'} color={'green'} size={20}/>
                        </Animatable.View>
                        : null}
                </View>
                <Text style={[styles.text_footer, {marginTop: 35}]}>Password</Text>
                <View style={styles.action}>
                    <Feather name={'lock'} color={"#05375a"} size={20}/>
                    <TextInput
                        placeholder={"Your password"}
                        secureTextEntry={data.secureTextEntry ? true : false}
                        style={styles.textInput}
                        autoCapitalize={'none'}
                        onChangeText={(val) => handlePasswordChange(val)}
                    />
                    <TouchableOpacity onPress={updateSecureTextEntry}>
                        {data.secureTextEntry ?
                        <Feather name={'eye-off'} color={'green'} size={20}/>
                        : <Feather name={'eye'} color={'green'} size={20}/>}
                    </TouchableOpacity>
                </View>
                {data.formIsCompletedError ?
                <View style={{marginTop: 10}}>
                    <Text style={{color: 'red', textAlign: 'center'}}>Please enter both email and password !</Text>
                </View> : null}
                {data.CredentialError ?
                    <View style={{marginTop: 10}}>
                        <Text style={{color: 'red', textAlign: 'center'}}>Credential Error</Text>
                    </View> : null}
                {data.isLoading ?
                <View style={{flex:1, justifyContent: 'center', alignItems: 'center'}}>
                    <ActivityIndicator size="large" color="#009387"/>
                </View>: null}
                <View style={styles.button}>
                    <TouchableOpacity style={styles.signIn} onPress={onSubmitEmailAuth}>
                        <LinearGradient colors={['#08d4c4', '#01ab9d']}
                                        style={styles.signIn}>
                            <Text style={styles.textSign}>Sign in</Text>
                        </LinearGradient>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={onSubmitGoogleAuth}
                                      style={[styles.signIn, {
                                          borderColor: '#009387',
                                          borderWidth: 1,
                                          marginTop: 15}]}>
                        <Text style={styles.textSign}>Sign up with Google</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => navigation.navigate('SignUpScreen')}
                                      style={[styles.signIn, {
                                          borderColor: '#009387',
                                          borderWidth: 1,
                                          marginTop: 15}]}>
                        <Text style={styles.textSign}>Sign up</Text>
                    </TouchableOpacity>
                </View>
            </Animatable.View>

        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#009387'
    },
    header: {
        flex: 1,
        justifyContent: 'flex-end',
        paddingHorizontal: 20,
        paddingBottom: 50
    },
    footer: {
        flex: 3,
        backgroundColor: '#fff',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingHorizontal: 20,
        paddingVertical: 30
    },
    text_header: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 30
    },
    text_footer: {
        color: '#05375a',
        fontSize: 18
    },
    action: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingBottom: 5
    },
    actionError: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#FF0000',
        paddingBottom: 5
    },
    textInput: {
        flex: 1,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: '#05375a',
    },
    errorMsg: {
        color: '#FF0000',
        fontSize: 14,
    },
    button: {
        alignItems: 'center',
        marginTop: 50
    },
    signIn: {
        width: '100%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10
    },
    textSign: {
        fontSize: 18,
        fontWeight: 'bold'
    }
});
