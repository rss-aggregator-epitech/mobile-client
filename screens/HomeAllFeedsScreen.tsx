import * as React from 'react'
import {
    FlatList, Picker, RefreshControl, SafeAreaView, ScrollView, StyleSheet, Text, View,
} from 'react-native'
import {Avatar, Button, Card, Paragraph, TextInput, Title} from 'react-native-paper'
import {
    useCurrentUserLazyQuery,
} from '../generated/hooks'
import { Article, WebsiteFragment } from '../generated/types'

export const HomeAllFeedsScreen = ({route, navigation}) => {
    const [fetchData,{ data, refetch, loading }] = useCurrentUserLazyQuery({ fetchPolicy:'no-cache'});
    const [refreshing, setRefreshing] = React.useState(false);
    const [websites, setWebsites] = React.useState<WebsiteFragment[]>([])
    const [entries, setEntries] = React.useState({
        search: '', websites_name: 'all', websites_array: []
    });

    React.useEffect(() => {
        fetchData();
        if (!data || !data.authenticatedUser) return

        let docs: WebsiteFragment[] = []
        let websites_names : string[] = [];

        for (const feed of data?.authenticatedUser?.feeds) {
            for (const item of feed?.websites) {
                if (typeof (item.title) != 'undefined') {
                    if (!websites_names.includes(item.title)) {
                        websites_names.push(item.title);
                    }
                }
            }
        }


        for (const feed of data?.authenticatedUser?.feeds) 
            docs = [...docs, ...feed.websites]

        if (docs.length > 0) {
            setEntries({
                ...entries,
                isLoading: false,
                websites_array: websites_names
            })
        }

        setWebsites(docs)
    }, [data])

    const handleSearchInput = (val) => {
        setEntries({
            ...entries,
            search: val
        })
    }

    const selectWebsiteHandler = (val) => {
        setEntries({
            ...entries,
            websites_name: val
        })
    }

    const renderItem = ({item}) => {
        if (entries.websites_name == 'all' || entries.websites_name == item.title) {
            return (
                <View>
                    {item.articles.map((props, key) => {
                        if (props.title.toLowerCase().includes(entries.search.toLowerCase()) || entries.search == '') {
                            return (
                                <View key={key} style={{borderStyle: 'dashed', borderColor: '#DCDCDC'}}>
                                    <Card>
                                        <Card.Title title={item.title}/>
                                        <Card.Content>
                                            <Title>{props.title}</Title>
                                            <Title>{props.read}</Title>
                                            <Paragraph>{props.contentSnippet.slice(0, 50)}</Paragraph>
                                            <Text>{props.pubDate.substring(0, props.pubDate.indexOf('+') + '+'.length - 1)}</Text>
                                        </Card.Content>
                                        {/*<Card.Cover source={{uri: props.image}} />*/}
                                        <Card.Actions>
                                            <Button onPress={() => {
                                                navigation.navigate('Visualize article', {
                                                    article: props,
                                                    website_id: item.id
                                                })
                                            }}>See article</Button>
                                            {props.read ?
                                                <Text>Already Read</Text>
                                                : null}
                                        </Card.Actions>
                                    </Card>
                                </View>
                            );
                        } else {
                            return null;
                        }
                    })}
                </View>
            )
        } else {
            return null;
        }
    }

    return (
        <SafeAreaView style={styles.container}>
                    <TextInput placeholder={"Search for an article"} onChangeText={(val) => handleSearchInput(val)}/>
                    {entries.websites_array.length > 0 ?
                        <Picker selectedValue={entries.websites_name} style={{alignSelf: 'stretch'}} onValueChange={(val) => selectWebsiteHandler(val)}>
                            <Picker.Item label={"All"} value={"all"}/>
                            {entries.websites_array.map((item, key) => {
                                return (
                                    <Picker.Item key={key} label={item} value={item}/>
                                );
                            })}
                        </Picker>:null}
                    <FlatList
                        data={websites}
                        renderItem={renderItem}
                        keyExtractor={item => item.id}
                        onRefresh={refetch}
                        refreshing={refreshing}
                        nestedScrollEnabled
                    />
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#696969',
        padding: 10,
    },
    titleStyle: {
        fontSize: 28,
        fontWeight: 'bold',
        textAlign: 'center',
        padding: 10,
    },
    textStyle: {
        fontSize: 16,
        textAlign: 'center',
        padding: 10,
    },
    touchableOpacityStyle: {
        position: 'absolute',
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        right: 30,
        bottom: 30,
    },
    touchableOpacity2Style: {
        position: 'absolute',
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        left: 30,
        bottom: 30,
    },
    floatingButtonStyle: {
        resizeMode: 'contain',
        width: 50,
        height: 50,
        //backgroundColor:'black'
    },
    item: {
        backgroundColor: '#009387',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
        height: 80
    },
    title: {
        fontSize: 32,
    },
});
