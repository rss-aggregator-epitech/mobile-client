import * as React from "react";
import {createStackNavigator} from "@react-navigation/stack";
import {MaterialCommunityIcons, Ionicons, FontAwesome} from "@expo/vector-icons";

import {HomeScreen} from "./HomeScreen";
import {DetailsScreen} from "./DetailsScreen";
import {ExploreScreen} from "./ExploreScreen";
import {ProfileScreen} from "./ProfileScreen";

import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import {SettingsScreen} from "./SettingsScreen";
import {FeedScreen} from "./FeedScreen";

const HomeStack = createStackNavigator();
const ProfileStack = createStackNavigator();
const FeedStack = createStackNavigator();
const Tab = createBottomTabNavigator();

export const MaintabScreen = () => {
    return (
        <Tab.Navigator>
            <Tab.Screen name={'Home'}
                        component={HomeStackScreen}
                        options={{
                            tabBarLabel: 'Home',
                            tabBarIcon: ({color}) => (
                                <MaterialCommunityIcons name={'home'} color={color} size={26}/>
                            )
                        }}/>
            <Tab.Screen name={'Feed'}
                        component={FeedStackScreen}
                        options={{
                            tabBarLabel: 'Feed',
                            tabBarIcon: ({color}) => (
                                <FontAwesome name={'feed'} color={color} size={26}/>
                            )
                        }}/>
            {/*<Tab.Screen name={'Notifications'}*/}
            {/*            component={DetailsStackScreen}*/}
            {/*            options={{*/}
            {/*                tabBarLabel: 'Updates',*/}
            {/*                tabBarIcon: ({color}) => (*/}
            {/*                    <MaterialCommunityIcons name={'bell'} color={color} size={26}/>*/}
            {/*                )*/}
            {/*            }}/>*/}
            <Tab.Screen name={'Profile'}
                        component={ProfileStackScreen}
                        options={{
                            tabBarLabel: 'Profile',
                            tabBarIcon: ({color}) => (
                                <MaterialCommunityIcons name={'account'} color={color} size={26}/>
                            )
                        }}/>
            {/*<Tab.Screen name={'Settings'}*/}
            {/*            component={SettingsStackScreen}*/}
            {/*            options={{*/}
            {/*                tabBarLabel: 'Settings',*/}
            {/*                tabBarIcon: ({color}) => (*/}
            {/*                    <Ionicons name={'settings'} color={color} size={26}/>*/}
            {/*                )*/}
            {/*            }}/>*/}
        </Tab.Navigator>

    );
}

const HomeStackScreen = ({navigation}) => {
    return (
        <HomeStack.Navigator screenOptions={{
            headerStyle: {
                backgroundColor: '#009387',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold'
            }
        }}>
            <HomeStack.Screen name={'Home'} component={HomeScreen}
                              options={{
                                  title: 'Overview',
                                  headerLeft: () => (
                                      <Ionicons name={'ios-menu'} size={25} onPress={() => {navigation.openDrawer()}}/>
                                  )}}
            />
        </HomeStack.Navigator>
    )
};

const ProfileStackScreen = ({navigation}) => {
    return (
        <ProfileStack.Navigator screenOptions={{
            headerStyle: {
                backgroundColor: '#009387',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold'
            }
        }}>
            <ProfileStack.Screen name={'Profile'} component={ProfileScreen}
                                 options={{
                                     headerLeft: () => (
                                         <Ionicons name={'ios-menu'} size={25} onPress={() => {navigation.openDrawer()}}/>
                                     )}}
            />
        </ProfileStack.Navigator>
    )
};

const FeedStackScreen = ({navigation}) => {
    return (
        <FeedStack.Navigator screenOptions={{
            headerStyle: {
                backgroundColor: '#009387',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold'
            }
        }}>
            <FeedStack.Screen name={'Feed'} component={FeedScreen}
                                  options={{
                                      headerLeft: () => (
                                          <Ionicons name={'ios-menu'} size={25} onPress={() => {navigation.openDrawer()}}/>
                                      )}}
            />
        </FeedStack.Navigator>
    )
};
