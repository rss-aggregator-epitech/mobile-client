import {ActivityIndicator, Linking, Text, View} from "react-native";
import * as React from "react";
import {useApollo} from "../contexts/ApolloContext";
import { Avatar, Button, Card, Title, Paragraph } from 'react-native-paper';
import {useCurrentUserQuery, useGetAllTagsQuery, useMarkArticlesReadMutation} from "../generated/hooks";
import {useEffect} from "react";

export const FeedArticleScreen = ({route, navigation}) => {

    const [MarkArticleAsRead] = useMarkArticlesReadMutation();
    const [isLoading, setIsLoading] = React.useState({
        isLoading: false,
        mark: false
    });

    const MarkAsReadHandler = async () => {
        let res = null;
        try {
            setIsLoading({
                ...isLoading,
                isLoading: true
            });
            res = await MarkArticleAsRead({
                variables: {
                    websiteId: route.params.website_id,
                    articleLink: route.params.article.link
                }
            })
        } catch (e) {
            setIsLoading({
                ...isLoading,
                isLoading: false
            });
            console.error(e);
        }
        setIsLoading({
            ...isLoading,
            isLoading: false
        });
    }
        useEffect(() => {
            setIsLoading({
                ...isLoading,
                isLoading: true
            })
            MarkAsReadHandler().then(() => {
                    setIsLoading({
                        ...isLoading,
                        isLoading: false
                    })
                }
            )
        }, [])
    return (
        <Card>
            {isLoading.isLoading ?
                <View style={{flex:1, justifyContent: 'center', alignItems: 'center'}}>
                    <ActivityIndicator size="large" color="#009387"/>
                </View>: null}
            <Card.Title title={route.params.article.title} />
            <Card.Content>
                <Paragraph>{route.params.article.contentSnippet}</Paragraph>

            </Card.Content>
            {(/(jpg|gif|png|JPG|GIF|PNG|JPEG|jpeg)$/.test(route.params.article.image)) ?
                <Card.Cover source={{uri: route.params.article.image}} />
             : null}
            <Card.Actions>
                <Button onPress={() => Linking.openURL(route.params.article.link)}>
                        Link to the article
                </Button>
                <Text>Credits : {route.params.article.author}</Text>
            </Card.Actions>
            <Card.Actions>
                <Text>Date : {route.params.article.pubDate.substring(0, route.params.article.pubDate.indexOf('+') + '+'.length - 1)}</Text>
            </Card.Actions>

        </Card>
    )
}
