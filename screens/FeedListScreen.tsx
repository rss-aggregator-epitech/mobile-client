import {
    Button,
    ScrollView,
    Text,
    View,
    Image,
    FlatList,
    StyleSheet,
    SafeAreaView,
    TouchableOpacity
} from "react-native";
import * as React from "react";
import Constants from 'expo-constants';
import {useCurrentUserQuery} from "../generated/hooks";
import {AntDesign, FontAwesome} from "@expo/vector-icons";


const DATA = [
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        title: 'First Item',
    },
    {
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
        title: 'Second Item',
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d72',
        title: 'Third Item',
    },
];

function Item({ name }) {
    return (
        <View>
            <Text style={styles.title}>{name}</Text>
        </View>
    );
}

export const FeedListScreen = ({navigation}) => {

    let user = useCurrentUserQuery({variables: {}, pollInterval: 2000})


    const clickHandler = () => {
        //function to handle click on floating Action Button
        // alert('Floating Button Clicked');
        navigation.navigate('Add a Feed');
    };

    const clickRefresh = () => {
        user = useCurrentUserQuery({variables: {}});
    }

    const renderItem = ({ item }) => (
        <View style={[styles.item, {flexDirection: 'row', justifyContent: 'space-between', alignSelf: 'stretch'}]}>
            <Item name={item.name} />
            <View style={{flexDirection: 'row', justifyContent: 'space-between',alignItems: 'center', alignContent: 'space-between',paddingHorizontal: 20}}>
                <TouchableOpacity  style={{paddingRight: 15}} onPress={() => {
                    navigation.navigate('Edit a Feed', {feedName: item.name, feedId: item.id})
                }}>
                    <FontAwesome name={"edit"} size={30}/>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {
                    navigation.navigate('Visualize feed', {feedName: item.name, feedId: item.id})
                }}>
                    <FontAwesome name={"arrow-circle-o-right"} size={30}/>
                </TouchableOpacity>
            </View>

        </View>
    // <TouchableOpacity onPress={() => {
    //         navigation.navigate('Edit a Feed', {feedName: item.name, feedId: item.id})
    //     }
    //     }>
    //         <Item name={item.name} />
    //     </TouchableOpacity>
    );

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.container}>
                <FlatList
                    data={user?.data?.authenticatedUser?.feeds}
                    renderItem={renderItem}
                    keyExtractor={item => item.id}
                />
                <TouchableOpacity
                    activeOpacity={0.7}
                    onPress={clickHandler}
                    style={styles.touchableOpacityStyle}>
                    <Image
                        // FAB using TouchableOpacity with an image
                        // For online image
                        // source={{
                        //     uri:
                        //         'https://raw.githubusercontent.com/AboutReact/sampleresource/master/plus_icon.png',
                        // }}
                        // For local image
                        source={require('../assets/float-add-icon.png')}
                        style={styles.floatingButtonStyle}
                    />
                </TouchableOpacity>
                {/*<TouchableOpacity*/}
                {/*    activeOpacity={0.7}*/}
                {/*    onPress={clickRefresh}*/}
                {/*    style={styles.touchableOpacity2Style}>*/}
                {/*    <FontAwesome name={"refresh"} size={30} style={{backgroundColor: "#fff", padding: 5, borderRadius: 10}}/>*/}
                {/*</TouchableOpacity>*/}
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: 10,
    },
    titleStyle: {
        fontSize: 28,
        fontWeight: 'bold',
        textAlign: 'center',
        padding: 10,
    },
    textStyle: {
        fontSize: 16,
        textAlign: 'center',
        padding: 10,
    },
    touchableOpacityStyle: {
        position: 'absolute',
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        right: 30,
        bottom: 30,
    },
    touchableOpacity2Style: {
        position: 'absolute',
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        left: 30,
        bottom: 30,
    },
    floatingButtonStyle: {
        resizeMode: 'contain',
        width: 50,
        height: 50,
        //backgroundColor:'black'
    },
    item: {
        backgroundColor: '#009387',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
        height: 80
    },
    title: {
        fontSize: 32,
    },
});
