import * as React from "react";
import {createStackNavigator} from "@react-navigation/stack";
import {FeedListScreen} from "./FeedListScreen";
import {FeedAddScreen} from "./FeedAddScreen";
import {EditFeedScreen} from "./EditFeedScreen";
import {FeedSeeScreen} from "./FeedSeeScreen";
import {FeedArticleScreen} from "./FeedArticleScreen";

const Stack = createStackNavigator();


export const FeedScreen = ({navigation}) => {

    return (
        <Stack.Navigator screenOptions={{
        headerTintColor: '#fff'
        }
        }>
            <Stack.Screen name={"FeedList"} component={FeedListScreen}/>
            <Stack.Screen name={"Add a Feed"} component={FeedAddScreen}/>
            <Stack.Screen name={"Edit a Feed"} component={EditFeedScreen}/>
            <Stack.Screen name={"Visualize feed"} component={FeedSeeScreen}/>
            <Stack.Screen name={"Visualize article"} component={FeedArticleScreen}/>
        </Stack.Navigator>
    )
}
