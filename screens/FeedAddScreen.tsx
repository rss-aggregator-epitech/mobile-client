import {
    Button,
    Text,
    View,
    StyleSheet,
    Platform,
    StatusBar,
    FlatList,
    ScrollView,
    SafeAreaView, TouchableOpacity, ActivityIndicator
} from "react-native";
import * as React from "react";
import {useApollo} from "../contexts/ApolloContext";
import {
    useCreateFeedMutation,
    useCurrentUserQuery,
    useGetAllTagsQuery,
    useGetAllWebsitesQuery,
    useGetWebsitesByTagQuery
} from "../generated/hooks";
import {Ionicons, AntDesign} from "@expo/vector-icons";
import {DefaultTheme, TextInput} from "react-native-paper";

const DATA = [
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        title: 'First Item',
    },
    {
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
        title: 'Second Item',
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d72',
        title: 'Third Item',
    },
];

const Item = ({ title}) => (
    <View style={styles.item}>
        <Text style={styles.title}>{title}</Text>
    </View>
);

const theme = {
    ...DefaultTheme,
    // Specify custom property
    myOwnProperty: true,
    // Specify custom property in nested object
    colors: {
        myOwnColor: '#009387',
    }
};

export const FeedAddScreen = ({navigation}) => {

    const user = useCurrentUserQuery({variables: {}})
    const [createFeed] = useCreateFeedMutation();
    const [entries, setEntries] = React.useState({
        feedName: '', isLoading: false
    })
    // const tags = useGetAllTagsQuery({variables: {}})

    // console.log(id[0].id);
    // const {data, loading, error} = useGetWebsitesByTagQuery({
    //     variables: {
    //         tagId: id[0].id
    //     }
    // })
    // console.log(tags?.data)


    const renderItem = ({ item }) => (
            <View style={{flex: 1, flexDirection: 'row'}}>
                <Item title={item.title} />
                <TouchableOpacity style={{padding: 40}}>
                    <AntDesign name={"plus"} size={20}/>
                </TouchableOpacity>
            </View>


    );

    const handleTextChange = (val) => {
        if (val.length > 0) {
            setEntries({
                ...entries,
                feedName: val
            })
        }
    }

    const onSubmit = async () =>  {
        let res = null;
        if (entries.feedName.length > 0) {
            try {
                setEntries({
                    ...entries,
                    isLoading: true
                })
                res = createFeed({
                    variables: {
                        userId: user?.data?.authenticatedUser?.id,
                        name: entries.feedName
                    }
                })
            } catch (e) {
                setEntries({
                    ...entries,
                    isLoading: false
                })
                console.error(e);
            }
            setEntries({
                ...entries,
                isLoading: false
            })
            alert("Feed created");
            navigation.navigate('FeedList');
        }
    }


    return (
        <SafeAreaView>
                <View>
                    {entries.isLoading ?
                        <View style={{flex:1, justifyContent: 'center', alignItems: 'center'}}>
                            <ActivityIndicator size="large" color="#009387"/>
                        </View>: null}
                    <TextInput
                        label={"Name of the Feed"} onChangeText={(val) => handleTextChange(val)}/>
                    <TouchableOpacity onPress={onSubmit}
                                      style={[styles.btn, {
                                          borderColor: '#009387',
                                          borderWidth: 1,
                                          marginTop: 15}]}>
                        <Text style={styles.text_btn}>Create a feed</Text>
                    </TouchableOpacity>
                    {/*<FlatList*/}
                    {/*    data={websites?.data?.allWebsites}*/}
                    {/*    renderItem={renderItem}*/}
                    {/*    keyExtractor={item => item.id}*/}
                    {/*/>*/}
                </View>
        </SafeAreaView>

    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
    },
    btn: {
        width: '100%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10
    },
    text_btn: {
        fontSize: 18,
        fontWeight: 'bold'
    },
    item: {
        backgroundColor: '#009387',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
        height: 80
    },
    title: {
        fontSize: 32,
    },
});
