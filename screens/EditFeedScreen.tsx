import {
    ActivityIndicator,
    FlatList,
    Image,
    Picker,
    SafeAreaView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from "react-native";
import * as React from "react";
import {useApollo} from "../contexts/ApolloContext";
import {
    useAddWebsiteToFeedMutation,
    useCurrentUserQuery,
    useDeleteFeedMutation,
    useGetAllTagsQuery,
    useGetAllWebsitesQuery,
    useGetUserFeedByIdQuery,
    useGetWebsitesByTagQuery,
    useRemoveWebsiteFromFeedMutation,
    useUpdateFeedMutation
} from "../generated/hooks";
import {Button, TextInput} from "react-native-paper";
import {FontAwesome, Ionicons, MaterialCommunityIcons} from "@expo/vector-icons";
import {Tag, Website, WebsiteFragment} from "../generated/types";

export const EditFeedScreen = ({route, navigation}) => {

    const forceUpdate: () => void = React.useState()[1].bind(null, {})
    const [AddWebsiteToFeed] = useAddWebsiteToFeedMutation();
    const [RemoveWebsiteFromFeed] = useRemoveWebsiteFromFeedMutation();
    const [UpdateFeed] = useUpdateFeedMutation();
    const [DeleteFeed] = useDeleteFeedMutation();
    const websites_tag = useGetAllTagsQuery({ fetchPolicy: 'no-cache' });
    const [websites, setWebsites] = React.useState<WebsiteFragment[]>([])
    const {data, refetch} = useGetAllWebsitesQuery({ fetchPolicy: 'no-cache' });
    const [all_tags, setAllTags] = React.useState<Tag[]>([]);
    const [isLoading, setIsLoading] = React.useState(true);
    const [error, setError] = React.useState({
        isError: false, errorMsg: ''
    });

    React.useEffect(() => {
        if (!data || !data.allWebsites) return

        let docs: WebsiteFragment[] = []

        docs = [...docs, ...data?.allWebsites];
        if (docs.length > 1) {
            setIsLoading(false);
        }
        setWebsites(docs);
    }, [data])

    React.useEffect(() => {
        if (!websites_tag || !websites_tag?.data?.allTags) return

        let docs: Tag[] = []

        docs = [...docs, ...websites_tag?.data?.allTags];

        setAllTags(docs);


    }, [websites_tag])

    let actual_websites: string[] = [];
    const params = route.params;
    const feed = useGetUserFeedByIdQuery({
        variables: {
            feedId: params.feedId
        }
    });

    feed?.data?.Feed?.websites.forEach((item) => {
        actual_websites.push(item?.title);
    })

    const [entries, setEntries] = React.useState({
        website_array: [], name: '', tag: 'all'
    })


    const AddWebsiteToFeedHandler = async (val) => {
        let res = null;
        try {
            setIsLoading(true);
            res = await AddWebsiteToFeed({
                variables: {
                    feedId: params.feedId,
                    websiteId: val
                }
            })
        } catch (e) {
            setIsLoading(false);
            console.error(e);
        }
        setIsLoading(false);
        alert("Website successfully added");
        forceUpdate();
        // console.log(res)
    }

    const RemoveWebsiteToFeedHandler = async (val) => {
        let res = null;
        try {
            res = await RemoveWebsiteFromFeed({
                variables: {
                    feedId: params.feedId,
                    websiteId: val
                }
            })
        } catch (e) {
            console.error(e);
        }
        alert("Website successfully deleted");
        forceUpdate();
    }

    const RemoveFeedHandler = async () => {
        let res = null;
        console.log("hello");
        try {
            res = await DeleteFeed({
                variables: {
                    feedId: params.feedId,
                }
            })
        } catch (e) {
            console.error(e);
            setErrorMsg('Remove could not be done'); return;
        }
        resetError();
        alert("Feed successfully deleted");
        navigation.goBack();
    }

    const setErrorMsg = (errorMsg : string) => {
        setError({
            ...error,
            isError: true,
            errorMsg: errorMsg
        })
    }

    const resetError = () => {
        setError({
            ...error,
            isError: false,
            errorMsg: ''
        })
    }

    const inputChangeHandler = (val) => {
        if (val.length > 0) {
            setEntries({
                ...entries,
                name: val
            })
        }
    }

    const RenameFeedHandler = async () => {
        if (entries.name.length == 0) {
            return;
        }
        let res = null;
        try {

            res = await UpdateFeed({
                variables: {
                    feedId: params.feedId,
                    name: entries.name
                }
            })

        } catch (e) {
            console.error(e);
            setErrorMsg('Rename could not be done, please verify if it is not the same as before'); return;
        }
        resetError();
        alert("Feed successfully renamed");
        navigation.navigate('FeedList');
    }

    const renderItem = ({ item }) => {
        if (!actual_websites.includes(item.title) && (entries.tag == 'all' || item.tag.name == entries.tag))
            return (
                <TouchableOpacity onPress={() => AddWebsiteToFeedHandler(item.id)}>
                    <View style={[styles.item, {flexDirection: 'row', justifyContent: 'space-between', alignSelf: 'stretch'}]}>
                        <Text style={styles.title}>{item.title}</Text>
                    </View>
                </TouchableOpacity>
            );
        else if (entries.tag == 'all' || item.tag.name == entries.tag){
        return (
                    <TouchableOpacity onPress={() => RemoveWebsiteToFeedHandler(item.id)}>
                        <View style={[styles.item_checked, {flexDirection: 'row', justifyContent: 'space-between', alignSelf: 'stretch'}]}>
                            <Text style={styles.title}>{item.title}</Text>
                        </View>
                    </TouchableOpacity>
        );}
    };

    const selectTagHandler = (val) => {
        setEntries({
            ...entries,
            tag: val
        })

    }

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.container}>

                <TextInput label={"Name of the feed : " + params.feedName} value={params.name} placeholder={params.name} onChangeText={(val) => inputChangeHandler(val)}/>
                <Picker selectedValue={entries.tag} mode={"dropdown"} style={{alignSelf: 'stretch'}} onValueChange={(val) => selectTagHandler(val)}>
                    <Picker.Item label={"All"} value={"all"}/>
                    {all_tags?.map((item, key) => {
                        return (
                          <Picker.Item key={key} label={item._label_} value={item.name}/>
                        );
                    })}
                </Picker>
                {isLoading ?
                    <View style={{flex:1, justifyContent: 'center', alignItems: 'center'}}>
                        <ActivityIndicator size="large" color="#009387"/>
                    </View>: null}
                <FlatList
                    data={websites}
                    renderItem={renderItem}
                    keyExtractor={item => item.id}
                />
                {error.isError ?
                    <Text style={{color: 'red'}}>{error.errorMsg}</Text>
                        : null
                }
                <Text style={{color: '#fff', textAlign: 'center'}}>Click on a card to add/remove a website</Text>
                <Text style={{color: '#009387', textAlign: 'center'}}>Available to pickup</Text>
                <Text style={{color: '#DC143C', textAlign: 'center'}}>Already owned</Text>
                <Button
                    icon="delete"
                    onPress={RemoveFeedHandler}>
                    Delete this feed
                </Button>
                <Button
                    icon="circle-edit-outline"
                    onPress={RenameFeedHandler}>
                    Rename this feed
                </Button>
            </View>

        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#696969',
        padding: 10,
        alignSelf: 'stretch'
    },
    titleStyle: {
        fontSize: 28,
        fontWeight: 'bold',
        textAlign: 'center',
        padding: 10,
    },
    textStyle: {
        fontSize: 16,
        textAlign: 'center',
        padding: 10,
    },
    item: {
        backgroundColor: '#009387',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
        height: 80
    },
    item_checked: {
        backgroundColor: '#DC143C',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
        height: 80
    },
    touchableOpacityStyle: {
        position: 'absolute',
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        right: 30,
        bottom: 30,
    },
    touchableOpacity2Style: {
        position: 'absolute',
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        left: 30,
        bottom: 30,
    },
    floatingButtonStyle: {
        resizeMode: 'contain',
        width: 50,
        height: 50,
        //backgroundColor:'black'
    },
    title: {
        fontSize: 20,
    }
});
