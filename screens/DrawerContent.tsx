import * as React from "react";
import {DrawerContentScrollView, DrawerItem} from "@react-navigation/drawer";
import {Title, Avatar, Caption, Paragraph, Drawer, Text, TouchableRipple, Switch} from "react-native-paper";
import {View, StyleSheet} from "react-native";
import {MaterialCommunityIcons, Ionicons, MaterialIcons, FontAwesome} from "@expo/vector-icons";
import {useApollo} from "../contexts/ApolloContext";
import {useCurrentUserQuery} from "../generated/hooks";

export function DrawerContent(props) {


    let firstname, lastname, email;
    const {data, loading, error} = useCurrentUserQuery({variables: {}})
    firstname = data?.authenticatedUser?.firstname;
    lastname = data?.authenticatedUser?.lastname;
    email = data?.authenticatedUser?.email;

    let apollo = useApollo();
    const signout = () => {
        apollo.removeToken();
    }

    return(
      <View style={{flex: 1}}>
          <DrawerContentScrollView {...props}>
              <View style={styles.drawerContent}>
                  <View style={styles.userInfoSection}>
                      <View style={{flexDirection: 'row', marginTop: 15}}>
                          <Avatar.Image
                            source={{
                                uri: 'https://image.freepik.com/vecteurs-libre/profil-avatar-homme-icone-ronde_24640-14044.jpg'
                            }}
                            size={50}/>
                            <View style={{marginLeft: 15, flexDirection: 'column'}}>
                                <Title style={styles.title}>{firstname} {lastname}</Title>
                                <Caption style={styles.caption}>{email}</Caption>
                            </View>
                      </View>
                  </View>
                  <Drawer.Section style={styles.drawerSection}>
                      <DrawerItem
                          icon={({color, size}) => (
                              <Ionicons name={'home-outline'} color={color} size={size}/>
                          )}
                          label={'Home'} onPress={() => {props.navigation.navigate('Home')}}
                      />
                      <DrawerItem
                          icon={({color, size}) => (
                              <FontAwesome name={'feed'} color={color} size={size}/>
                          )}
                          label={'Feed'} onPress={() => {props.navigation.navigate('Feed')}}
                      />
                      <DrawerItem
                          icon={({color, size}) => (
                              <MaterialCommunityIcons name={'account-outline'} color={color} size={size}/>
                          )}
                          label={'Profile'} onPress={() => {props.navigation.navigate('Profile')}}
                      />
                  </Drawer.Section>
              </View>
          </DrawerContentScrollView>
          <Drawer.Section style={styles.bottomDrawerSection}>
              <DrawerItem
                  icon={({color, size}) => (
                      <MaterialIcons name={'exit-to-app'} color={color} size={size}/>
                      )}
                  label={'Sign Out'} onPress={signout}/>
          </Drawer.Section>
      </View>
    );
}

const styles = StyleSheet.create({
    drawerContent: {
        flex: 1,
    },
    userInfoSection: {
        paddingLeft: 20,
    },
    title: {
        fontSize: 16,
        marginTop: 3,
        fontWeight: 'bold',
    },
    caption: {
        fontSize: 14,
        lineHeight: 14,
    },
    row: {
        marginTop: 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    section: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 15,
    },
    paragraph: {
        fontWeight: 'bold',
        marginRight: 3,
    },
    drawerSection: {
        marginTop: 15,
    },
    bottomDrawerSection: {
        marginBottom: 15,
        borderTopColor: '#f4f4f4',
        borderTopWidth: 1
    },
    preference: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 12,
        paddingHorizontal: 16,
    },
});
