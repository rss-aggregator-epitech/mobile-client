import { createUploadLink } from 'apollo-upload-client'
import Constants from 'expo-constants'
import React, { createContext, useContext, useEffect, useState } from 'react'

import { ApolloClient, ApolloProvider, InMemoryCache, NormalizedCacheObject } from '@apollo/client'
import { setContext } from '@apollo/client/link/context'
import AsyncStorage from '@react-native-async-storage/async-storage'

const { manifest } = Constants

const endpoint = process.env.EXPO_LOCAL_SERVER
  ? `http://${manifest.debuggerHost?.split(':').shift()}:8000`
  : 'https://rss-aggregator-server-prod.herokuapp.com'

const uri = `${endpoint}/admin/api`

const TOKEN_KEY = 'token'
const httpLink = createUploadLink({ uri, credentials: 'include' })

type ApolloContextType = {
  client: ApolloClient<NormalizedCacheObject>
  setToken: (token: string | null)=> void
  removeToken: ()=> void
  token: string | null
  isSignedIn: boolean
}

// @ts-ignore
export const ApolloContext = createContext<ApolloContextType>({})

export const ApolloContextProvider: React.FC = ({ children }) => {
  const [token, setToken] = useState<string | null>(null)

  useEffect(() => {
    AsyncStorage.getItem(TOKEN_KEY).then(_token => {
      if (_token)
        setToken(_token)
    })
  }, [])

  useEffect(() => {
    if (token)
      AsyncStorage.setItem(TOKEN_KEY, token)
    else
      AsyncStorage.removeItem(TOKEN_KEY)
  }, [token])

  const removeToken = () => {
    setToken(null)
  }

  const memoryCache = new InMemoryCache({
    typePolicies: {
      User: {
        fields: {
          favorites: {
            merge(existing, incoming) {
              return [...incoming]
            },
          },
        },
      },
    },
  })

  const authLink = setContext((_, { headers }) => {
    return {
      headers: {
        ...headers,
        authorization: token ? `Bearer ${token}` : '',
      },
    }
  })

  const client = new ApolloClient({
    cache: memoryCache,
    link: authLink.concat(httpLink),
  })

  return (
    <ApolloContext.Provider value={{
      client,
      setToken,
      removeToken,
      token,
      isSignedIn: !!token,
    }}
    >
      <ApolloProvider client={client}>
        {children}
      </ApolloProvider>
    </ApolloContext.Provider>
  )
}

export const useApollo = () => {
  const ctx = useContext(ApolloContext)
  return ctx
}
