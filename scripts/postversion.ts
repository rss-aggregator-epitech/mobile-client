import { readFileSync, writeFileSync } from 'fs'

(async () => {
let code = 0

try {
  const packageJSON = JSON.parse(readFileSync('./package.json', 'utf8'))
  const app = JSON.parse(readFileSync('./app.json', 'utf8'))

  app.expo.version = packageJSON.version

  writeFileSync('./app.json', JSON.stringify(app, null, 2))
} catch (e) {
  console.error(e)
  code = 1
}

process.exit(code)
})()
