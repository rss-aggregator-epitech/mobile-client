import axios from 'axios'

(async () => {
let code = 0

try {
  // @ts-ignore
  const [_, __, endpoint, version, branch] = process.argv
  const authorization = process.env.SECRET

  if (!authorization) throw new Error('Missing CI secret')
  if (!endpoint) throw new Error('Missing endpoint')
  if (!version) throw new Error('Missing version')
  if (!branch || (branch !== 'master' && branch !== 'staging')) throw new Error('Missing or invalid branch name')

  const channel = `${branch === 'master' ? 'prod' : 'staging'}-v${version}`
  const project = branch === 'master' ? 'rss-aggregator' : 'rss-aggregator-staging'
  const url = `https://expo.io/@kerma2/projects/${project}?release-channel=${channel}`

  const config = {
    headers: {
      authorization,
    },
  }

  await axios.post(`${endpoint}/admin/api`, {
    query: `
      mutation($type: ReleaseTypeType!, $version: String!, $url: String!) {
        updateReleaseURL(type: $type, version: $version, url: $url) 
      }
    `,
    variables: { type: 'mobile', version, url },
  }, config)

  console.log('done')
} catch (e) {
  code = 1
  console.error(e)
}

process.exit(code)
})()
